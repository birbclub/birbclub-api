FROM golang AS build-env

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY cmd/main/main.go ./cmd/main/
COPY cmd/main/options.go ./cmd/main/
COPY internal/api/api.go ./internal/api/
COPY internal/builtin/api/v1/stat.go ./internal/builtin/api/v1/
COPY internal/builtin/api/v1/v1.go ./internal/builtin/api/v1/
COPY internal/builtin/server/server.go ./internal/builtin/server/
COPY internal/server/server.go ./internal/server/

RUN CGO_ENABLED=0 go build -o /birbclub-api birbclub-api/cmd/main

FROM scratch

WORKDIR /
COPY --from=build-env /birbclub-api /birbclub-api

EXPOSE 8080

ENTRYPOINT ["/birbclub-api"]
