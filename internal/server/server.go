package server

type Options struct {
	Endpoint   string
	ServerHost string
	ServerPort uint16
}

type Interface interface {
	Serve()
	Options() *Options
}
