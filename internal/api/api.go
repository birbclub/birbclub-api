package api

import (
	"birbclub-api/internal/server"
)

type Options struct {
	Server server.Interface
}
