package v1

import (
	"birbclub-api/internal/api"
	"github.com/labstack/echo/v4"
)

type Api struct {
	options *api.Options
}

func New(options *api.Options) *Api {
	return &Api{
		options: options,
	}
}

func (api *Api) Stat(ctx echo.Context) error {
	response, err := stat(api)

	if err != nil {
		ctx.Logger().Error("Unable to get server stats: ", err)
		return ctx.NoContent(400)
	}

	return ctx.JSON(200, response)
}
