package v1

import "github.com/alteamc/minequery/ping"

type statResponse struct {
	Version string   `json:"version"`
	Players []string `json:"players"`
}

func stat(api *Api) (*statResponse, error) {
	serverOptions := api.options.Server.Options()

	pingResult, err := ping.Ping(serverOptions.ServerHost, serverOptions.ServerPort)

	if err != nil {
		return nil, err
	}

	response := &statResponse{
		Version: pingResult.Version.Name,
		Players: make([]string, len(pingResult.Players.Sample)),
	}

	for idx, player := range pingResult.Players.Sample {
		response.Players[idx] = player.Name
	}

	return response, nil
}
