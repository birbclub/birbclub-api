package server

import (
	"birbclub-api/internal/api"
	"birbclub-api/internal/builtin/api/v1"
	"birbclub-api/internal/server"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Server struct {
	options *server.Options
	echo    *echo.Echo
}

func New(options *server.Options) server.Interface {
	return &Server{
		options: options,
		echo:    echo.New(),
	}
}

func (s *Server) Serve() {
	s.echo.Use(middleware.Logger())
	s.echo.Use(middleware.Recover())
	s.echo.Use(middleware.CORS())

	apiOptions := &api.Options{
		Server: s,
	}

	// v1 api
	apiv1 := v1.New(apiOptions)
	s.echo.GET("/v1/stat", apiv1.Stat)

	s.echo.Logger.Fatal(s.echo.Start(s.options.Endpoint))
}

func (s *Server) Options() *server.Options {
	return s.options
}
