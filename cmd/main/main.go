package main

import (
	"birbclub-api/internal/builtin/server"
)

func main() {
	server.New(getOptions()).Serve()
}
