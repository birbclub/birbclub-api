package main

import (
	"birbclub-api/internal/server"
	"os"
	"strconv"
)

func getOptions() *server.Options {
	return &server.Options{
		ServerHost: getMinecraftHost(),
		ServerPort: getMinecraftPort(),
		Endpoint:   ":8080",
	}
}

func getMinecraftHost() string {
	serverHost := os.Getenv("BIRBCLUB_MINECRAFT_HOST")

	if serverHost == "" {
		panic("BIRBCLUB_MINECRAFT_HOST is required")
	}

	return serverHost
}

func getMinecraftPort() uint16 {
	serverPortStr := os.Getenv("BIRBCLUB_MINECRAFT_PORT")
	serverPort := uint16(25565)

	if serverPortStr != "" {
		value, err := strconv.ParseUint(serverPortStr, 10, 16)

		if err != nil {
			panic("BIRBCLUB_MINECRAFT_PORT is invalid number")
		}

		serverPort = uint16(value)
	}

	return serverPort
}
